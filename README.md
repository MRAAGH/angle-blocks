# Angle Blocks

![A pile of 3D printed blocks](http://mazie.rocks/img/fischerblocks-small.jpg)

This on Thingiverse: https://www.thingiverse.com/thing:4339870

Reverse-engineered to be compatible with Fischertechnik.

Angles and thickness are adjustable.

Usually printing with 1 wall and no infill is fine,
but if you want them to handle some load, use 2 walls and 20% infill!

To avoid headaches, you should print these on hot glass, *without* a brim,
*without* a part cooling fan, no more than 10 pieces at once
and with z-hopping enabled.

I don't recommend the knob;
you'll be better off using the "rail" models.
Knob is included for those who really need it,
but is weaker and harder to print.
