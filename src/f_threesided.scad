// This file was written by MRAAGH.
// It is released into the public domain.  Do whatever you want!
// An angled block with two Fischertechnik-compatible notches and a rail!

// Level of detail
$fn = 30;

// The tilt angle in degrees
angle = 10; // [0:30]

use <notch.scad>;
use <rail.scad>;

linear_extrude(15){
difference(){
hull(){
translate([15,0])rotate(angle)square([0.0001,15]);
rotate(-angle)square([0.0001,15]);
}
translate([15/2,0])notch();
rotate(90-angle){

translate([15/2,0])rotate(180)notch();
}
}

translate([15,0])rotate(90+angle)translate([15/2,0])rotate(180)rail();

}

