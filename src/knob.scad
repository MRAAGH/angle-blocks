// This file was written by MRAAGH.
// It is released into the public domain.  Do whatever you want!

module knob(){
  translate([0,0.3,0])cube([2.5,1,2.5], center=true);
  translate([0,2.3,0])intersection(){
    translate([0,0,-10])cylinder(h=20,r=2.09);
    rotate([0,90,0])translate([0,0,-10])cylinder(h=20,r=2.09);
    translate([0,-4.7,3.15])cube([10,10,10],center=true);
  }
}


