// This file was written by MRAAGH.
// It is released into the public domain.  Do whatever you want!
// An angled block with a Fischertechnik-compatible notch and knob!
// Print with supports!

// Level of detail
$fn = 30;

// The tilt angle in degrees
angle = 20; // [0:90]

// The height (thickness) of the block, if you measure it in the middle
height = 5; // [0:50]

use <notch.scad>;
use <knob.scad>;

linear_extrude(15)
difference(){
hull(){
rotate(-angle/2)square([15,0.0001],center=true);
translate([0,height])rotate(angle/2)square([15,0.0001],center=true);
}
rotate(-angle/2)notch();
}
translate([0,height,7.5])rotate(angle/2)knob();
