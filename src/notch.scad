// This file was written by MRAAGH.
// It is released into the public domain.  Do whatever you want!

module notch_round(){
  union(){
    translate([0,2.26])difference(){
      circle(r=2.17);
    }
    square([2.8,2], center=true);
  }
}

module notch(){
  union(){
    translate([0,2.26])difference(){
      circle(r=2.17);
      translate([0,2.5])square([100,4.0],center=true);
    }
    square([2.8,2], center=true);
  }
}
