// This file was written by MRAAGH.
// It is released into the public domain.  Do whatever you want!

module rail(){
  union(){
    translate([0,0.3])square([2.5,1], center=true);
    translate([0,2.36])intersection(){
      difference(){
        translate([0,0])circle(r=2.11);
        translate([0,0])circle(r=0.8);
      }
      translate([0,-4.7])square([10,10],center=true);
    }
  }
}

module rail_flat(){
  union(){
    translate([0,0.3])square([2.5,1], center=true);
    translate([0,2.3])intersection(){
      translate([0,0])circle(r=2.12);
      translate([0,-4.7])square([10,10],center=true);
    }
  }
}
