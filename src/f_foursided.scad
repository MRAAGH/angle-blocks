// This file was written by MRAAGH.
// It is released into the public domain.  Do whatever you want!
// An angled block with Fischertechnik-compatible notches on four sides!

// Level of detail
$fn = 30;

// The tilt angle in degrees
angle = 15; // [0:45]

use <notch.scad>;

linear_extrude(15)
difference(){
polygon([[0,0],[0,15],[15,15+tan(angle)*15],[15,0]]);
translate([7.5,0])notch();
translate([0,7.5])rotate(-90)notch();
translate([15,7.5])rotate(90)notch();
translate([0,15])rotate(180)rotate(angle)translate([-7.5,0])notch();
}

