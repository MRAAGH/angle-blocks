// This file was written by MRAAGH.
// It is released into the public domain.  Do whatever you want!
// A two-way-angled block with Fischertechnik-compatible notch and rail!

// Level of detail
$fn = 30;

// The horizontal tilt angle in degrees
angle = 20; // [0:90]

// The vertical tilt angle in degrees
angle2 = 40; // [0:90]

// The height (thickness) of the block, if you measure it in the middle
height = 10; // [0:50]

use <notch.scad>;
use <rail.scad>;

module body(a, a2, h){
hull(){
rotate([-a2/2,0,-a/2])
cube([15,0.0001,15],center=true);
translate([0,h,0])rotate([a2/2,0,a/2])
cube([15,0.0001,15],center=true);
}
}

difference(){
body(angle,angle2,height);
rotate([-angle2/2,0,-angle/2])
linear_extrude(100,center=true)notch();

}

intersection(){
translate([0,height,0])rotate([angle2/2,0,angle/2])
linear_extrude(100,center=true,convexity=2)rail();
// need to intersect rail with extended body
body(angle,angle2,height*2);
}
